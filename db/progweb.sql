-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 04, 2014 at 09:28 AM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `progweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `bmx`
--

CREATE TABLE IF NOT EXISTS `bmx` (
  `id_bmx` int(3) NOT NULL AUTO_INCREMENT,
  `judul` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `isi` varchar(130) NOT NULL,
  `oleh` varchar(30) NOT NULL,
  `id_member` int(3) NOT NULL,
  PRIMARY KEY (`id_bmx`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `bmx`
--

INSERT INTO `bmx` (`id_bmx`, `judul`, `tanggal`, `isi`, `oleh`, `id_member`) VALUES
(1, 'halo', '2014-12-01 21:02:36', 'ini isiinya', 'admin', 1),
(7, 'Progweb', '2014-12-01 06:34:55', 'halo halo halo halo ', 'admin', 1),
(5, 'Abadi', '2014-12-01 06:30:31', 'Strees\r\n', 'admin', 1),
(6, 'Tetst', '2014-12-01 06:31:13', 'asdasdasd', 'admin', 1),
(8, 'asdasdas', '2014-12-01 08:41:47', 'asdasd', 'admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `id_member` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `status` varchar(15) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id_member`, `id_barang`, `status`, `foto`) VALUES
(3, 4, 'order', '10635876_10203787849115398_8950190888520747791_n.jpg'),
(2, 7, 'order', 'hitem2.jpg'),
(3, 5, 'order', 'bike2.jpg'),
(3, 5, 'order', 'bike2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `classic`
--

CREATE TABLE IF NOT EXISTS `classic` (
  `id_classic` int(3) NOT NULL AUTO_INCREMENT,
  `judul` varchar(40) NOT NULL,
  `tanggal` datetime NOT NULL,
  `isi` varchar(120) NOT NULL,
  `oleh` varchar(30) NOT NULL,
  `id_member` int(3) NOT NULL,
  PRIMARY KEY (`id_classic`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `classic`
--

INSERT INTO `classic` (`id_classic`, `judul`, `tanggal`, `isi`, `oleh`, `id_member`) VALUES
(1, 'asdasda', '2014-12-01 08:42:33', 'asdasdsa', 'admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `com_name` varchar(100) NOT NULL,
  `come_email` varchar(100) NOT NULL,
  `com_dis` varchar(100) NOT NULL,
  `post_id` int(10) NOT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--


-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `id_event` int(2) NOT NULL AUTO_INCREMENT,
  `judul_event` varchar(15) NOT NULL,
  `tgl` date NOT NULL,
  `waktu` varchar(20) NOT NULL,
  `tempat` varchar(15) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `dari` varchar(30) NOT NULL,
  `id_member` int(2) NOT NULL,
  PRIMARY KEY (`id_event`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id_event`, `judul_event`, `tgl`, `waktu`, `tempat`, `foto`, `deskripsi`, `dari`, `id_member`) VALUES
(17, 'opopopo', '2012-12-12', '20:02:23', 'sdasdasdasd', '1914-2.jpg', 'dfsdfsdfsdf', '71120067', 4),
(18, 'test', '2012-12-12', '12:12', 'ASDASD', '11.PNG', 'adasdasdasd', 'admin', 1),
(19, 'asdasd', '2013-12-12', '12:00', 'asdasdasd', '13.PNG', 'asdasdasdsads', 'admin', 1),
(20, 'HALOO', '2014-01-12', '11:11', 'asdasdasd', 'cgb17acousticguitarecon.jpg', 'asdasdasdasd', 'admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fixie`
--

CREATE TABLE IF NOT EXISTS `fixie` (
  `id_fixie` int(4) NOT NULL AUTO_INCREMENT,
  `judul` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `isi` varchar(120) NOT NULL,
  `oleh` varchar(30) NOT NULL,
  `id_member` int(4) NOT NULL,
  PRIMARY KEY (`id_fixie`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `fixie`
--

INSERT INTO `fixie` (`id_fixie`, `judul`, `tanggal`, `isi`, `oleh`, `id_member`) VALUES
(1, 'asdasdasd', '2014-12-01 08:45:39', 'asdasda', 'admin', 1),
(2, 'breki ', '2014-12-01 08:45:44', 'asu', 'admin', 1),
(3, 'Progweb ', '2014-12-01 08:48:31', 'telat\r\n', 'admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `id_member` int(3) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(20) NOT NULL,
  `name` varchar(30) NOT NULL,
  `age` int(2) NOT NULL,
  `sex` varchar(7) NOT NULL,
  `email` varchar(30) NOT NULL,
  `address` varchar(50) NOT NULL,
  `regional` varchar(15) NOT NULL,
  `community` varchar(15) NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_member`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id_member`, `username`, `password`, `name`, `age`, `sex`, `email`, `address`, `regional`, `community`, `photo`) VALUES
(1, 'admin', 'admin', 'Breki the Breaker', 22, 'male', 'admin@admin.com', 'ukdw', 'jogja', 'bmx', '1417338567_settings.png'),
(2, 'member', 'member', 'Abadi Susanto', 33, 'female', 'asdasd@asd.com', 'rumah', 'jogja', 'classic', '1880-2.jpg'),
(3, 'damar', 'asdasd', 'Damar Santoso asdasd', 23, 'female', 'asds@asd.asd', 'ada deh', 'mau tau aja ', '', '1905-2.jpg'),
(4, '71120067', 'akuganteng', 'Arya Beth', 20, 'male', 'abadi_kaka@yahoo.com', 'Jalan Kaliurang', 'Jogja', 'bmx', '1908-2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `mountain`
--

CREATE TABLE IF NOT EXISTS `mountain` (
  `id_mountain` int(4) NOT NULL AUTO_INCREMENT,
  `judul` varchar(40) NOT NULL,
  `tanggal` datetime NOT NULL,
  `isi` varchar(120) NOT NULL,
  `oleh` varchar(20) NOT NULL,
  `id_member` int(4) NOT NULL,
  PRIMARY KEY (`id_mountain`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `mountain`
--

INSERT INTO `mountain` (`id_mountain`, `judul`, `tanggal`, `isi`, `oleh`, `id_member`) VALUES
(1, 'asdasdas', '2014-12-01 08:49:31', 'asdasd', 'admin', 1),
(2, 'asdasdas', '2014-12-01 08:49:50', 'asdasd', 'admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE IF NOT EXISTS `penjualan` (
  `id_barang` int(2) NOT NULL AUTO_INCREMENT,
  `nama_barang` varchar(15) NOT NULL,
  `harga` int(6) NOT NULL,
  `stock` int(2) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `deskripsi` varchar(30) DEFAULT NULL,
  `testimoni` varchar(30) DEFAULT NULL,
  `foto` varchar(100) NOT NULL,
  `oleh` varchar(100) NOT NULL,
  `id_testi` int(2) DEFAULT NULL,
  `id_member` int(2) NOT NULL,
  PRIMARY KEY (`id_barang`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id_barang`, `nama_barang`, `harga`, `stock`, `no_hp`, `deskripsi`, `testimoni`, `foto`, `oleh`, `id_testi`, `id_member`) VALUES
(3, 'asdasdasd', 200000, 21, '082348234238483', 'asdasdsa', NULL, '1914-2.jpg', '71120067', NULL, 4),
(4, 'test', 2000000, 5, '082349234234', 'asdasdasd', NULL, '10635876_10203787849115398_8950190888520747791_n.jpg', 'admin', NULL, 1),
(5, 'Sepeda', 4000000, 5, '0', 'Sepeda gunung paling cocok ..', '09342234234', 'bike2.jpg', 'admin', NULL, 1),
(7, 'Senter', 100000, 4, '', 'adsadas', NULL, 'hitem2.jpg', 'member', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `teman`
--

CREATE TABLE IF NOT EXISTS `teman` (
  `id_relational` int(3) NOT NULL AUTO_INCREMENT,
  `id_member` int(3) NOT NULL,
  `id_teman` int(3) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `foto` varchar(100) NOT NULL,
  PRIMARY KEY (`id_relational`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `teman`
--

INSERT INTO `teman` (`id_relational`, `id_member`, `id_teman`, `nama`, `foto`) VALUES
(4, 2, 1, 'Breki the Breaker', '1417338567_settings.png'),
(5, 3, 1, 'Breki the Breaker', '1417338567_settings.png'),
(6, 3, 2, 'Abadi Susanto', '1880-2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `timeline`
--

CREATE TABLE IF NOT EXISTS `timeline` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tweet` varchar(140) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nama` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `community` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `id_member` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=77 ;

--
-- Dumping data for table `timeline`
--

INSERT INTO `timeline` (`id`, `tweet`, `dt`, `nama`, `foto`, `community`, `id_member`) VALUES
(73, 'haloo boy ', '2014-12-04 03:13:18', 'admin', '1907-2.jpg', 'bmx', 1),
(74, 'im admin here', '2014-12-04 03:13:24', 'admin', '1907-2.jpg', 'bmx', 1),
(75, 'haloo jg', '2014-12-04 03:13:51', 'member', '1907-2.jpg', 'fixie', 2),
(76, 'new foto and my name ', '2014-12-04 07:40:14', 'admin', '1417338567_settings.png', 'bmx', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
