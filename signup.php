<?php
	session_start();
	if(isset($_SESSION['name']))
	{
		header( "Location:index.php");

	}
?>	

<!DOCTYPE html>
<html>
<head>
	<title>Bike Club Community - Sign Up</title>
	<?php
		include "part/head.php";
	?>
</head>

<body>
	<div id="container">
		<div id="login">
			<?php
				include "part/topside.php"
			?>
		</div>
		<div id="header">
			<?php
				include "part/header.php";
			?>
		</div>

		<div id="slideshow">
			<?php
				include "part/slideshow.php";
			?>
		</div>

		<div id="sidebar">
			<?php
				include "part/sidebar.php";
			?>
		</div>

		<div id="content">
			<div class="isi">
			<h2>Sign Up</h2><hr>
			<form method="post" action="register.php" enctype="multipart/form-data">
				<table>
					<tr>
						<td>Username</td>
						<td><input type="text" name="username" required></td>
					</tr>
					<tr>
						<td>Password</td>
						<td><input type="password" name="password" required></td>
					</tr>
					<tr>
						<td>Retype Password</td>
						<td><input type="password" name="password2" required></td>
					</tr>
					<tr>
						<td colspan="2"><hr></td>
					</tr>
					<tr>
						<td>Full Name</td>
						<td><input type="text" name="fullname" placeholder="ex : Andreas Isnawan" required></td>
					</tr>
					<tr>
						<td>Age</td>
						<td><input type="text" name="age" placeholder="ex : 23" required></td>
					</tr>
					<tr>
						<td>Sex</td>
						<td><input type="radio" name="sex" value="male"> male
						<input type="radio" name="sex" value="female"> female</td>
					</tr>
					<tr>
						<td>Email</td>
						<td><input type="text" name="email" placeholder="ex : your@email.com" required></td>
					</tr>
					<tr>
						<td>Address</td>
						<td><input type="text" name="address" placeholder="ex :Jl. Kaliurang km. 7" required></td>
					</tr>
					<tr>
						<td>Regional</td>
						<td><input type="text" name="regional" placeholder="ex : Jogjakarta" required></td>
					</tr>
					<tr>
						<td>Community</td>
						<td><input type="radio" name="community" value="bmx">BMX
						<input type="radio" name="community" value="classic">Classic
						<input type="radio" name="community" value="Fixie">Fixie
						<input type="radio" name="community" value="Mountain">Mountain</td>
					</tr>
					<tr>
						<td>Photo</td>
						<td><input type="file" name="photo" required></td>
					</tr>
					<tr>
						<td>Type the captcha</td>
						<td><img src="captcha.php" width="100" height="30" alt="captcha" /></td>
					</tr>
					<tr>
						<td><label> </label></td>
						<td><input style="width:90px;" id="captcha" type="text" class="text" placeholder="captcha here" name="captcha" size="6" maxlength="6" required></td>
					</tr>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td><input type="submit" name="Submit" class="button" value="CONFIRM">&nbsp;&nbsp;&nbsp;&nbsp; <input type="reset" name ="reset2" class="button" value="RESET"></td>
					</tr>
				</table>
			</form>
			</div>
		</div>

		
	</div>
	<div id="footer">
		<?php
			include "part/footer.php";
		?>
	</div>
	


</body>
</html>