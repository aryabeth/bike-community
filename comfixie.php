<?php
	session_start();
?>	

<!DOCTYPE html>
<html>
<head>
	<title>Bike Club Community - Classic</title>
	<?php
		include "part/head.php";
	?>
</head>

<body>
	<div id="container">
		<div id="login">
			<?php
				include "part/topside.php"
			?>
		</div>
		<div id="header">
			<?php
				include "part/header.php";
			?>
		</div>

		<div id="slideshow">
			<?php
				include "part/slideshow.php";
			?>
		</div>

		<div id="sidebar">
			<?php
				include "part/sidebar.php";
			?>
		</div>

		<div id="content">
			<div class="isi">
				<h2> THREAD BMX </h2><hr>

				<table class='tblThread'>
									<tr>
										<td><h3>No</h3></td>
										<td><h3>Judul Thread</h3></td>
										<td><h3>Thread Starter</h3></td>
									</tr>
				<?php
					if (isset($_SESSION['name'])) {
						echo "<a href='addFixie.php'>Tambah Thread</a>";
					}

						$count = 15;
						if(isset($_GET['page']))
							$page = $_GET['page'];
						else $page = 1;
						$limit = $count*($page-1);

						$query = "SELECT * FROM fixie ORDER BY id_fixie LIMIT ".$limit.",".$count."";
						$result = mysql_query($query);
						while($rows = mysql_fetch_assoc($result))
						{
							echo "
									<tr class='trThread'>
										<td>".$rows['id_fixie']."</td>
										<td>".$rows['judul']."</td>
										<td>".$rows['oleh']."</td>
										<td><a href='thread.php?id=".$rows['id_fixie']."'>Lihat</a></td>
									</tr>

								";
							
						}
						
				?>

				</table>

				<?php

						$query = "SELECT * FROM classic";
						$result = mysql_query($query);
						$total = mysql_num_rows($result);

						$num_page = ceil($total/$count);

						echo "<div id='halaman' align ='center'>";
						function pagination($page,$num_page)
						{
						  	for($i=1;$i<=$num_page;$i++) {
						     	if($i==$page){
									echo $i." ";
								}
								else
								{
							 		echo "<a href='comfixie.php?page=".$i."'' class='pagination'>".$i."</a> ";
								}
							}
						}

						if($num_page>1)
							pagination($page, $num_page);
						echo "</div>";


				?>
			
			</div>
		</div>
	</div>
	
	<div id="footer">
		<?php
			include "part/footer.php";
		?>
	</div>

</body>
</html>