<?php
	session_start();
?>	

<!DOCTYPE html>
<html>
<head>
	<title>Bike Club Community - Contact Us</title>
	<?php
		include "part/head.php";
	?>
</head>

<body>
	<div id="container">
		<div id="login">
			<?php
				include "part/topside.php"
			?>
			
		</div>
		<div id="header">
			<?php
				include "part/header.php";
			?>
		</div>

		<div id="slideshow">
			<?php
				include "part/slideshow.php";
			?>
		</div>

		<div id="sidebar">
			<?php
				include "part/sidebar.php";
			?>
		</div>

		<div id="content">
			<div class="isi">
				<h2>Contact Us</h2><hr>
				<p>We are welcome to all question, submit event, criticism, and message from you, please submit it by contact us at contact menu. Thank you for visiting us and happy shopping.</p>
				<form action="contact.php" method="post" id="contact">
				<table id="form">
					<tr>
						<td>Full Name</td>
						<td><input id="fullname" type="text" name="fullname" placeholder="ex : Andreas Isnawan" required></td>
					</tr>
					<tr>
						<td>Email</td>
						<td><input id="email" type="text" name="email" placeholder="ex : example@mail.com" required></td>
					</tr>
					<tr>
						<td>Subject</td>
						<td><input id="subject" type="text" name="subject" placeholder="ex : Question" required></td>
					</tr>
					<tr>
						<td>Message</td>
						<td><textarea id="msg" name="msg" rows="4" cols="50" placeholder="Your Message type here . . ." required></textarea></td>
					</tr>
					<tr>
						<td>Type the captcha</td>
						<td><img src="captcha.php" width="100" height="30" alt="capcha" /></td>
					</tr>
					<tr>
						<td><label> </label></td>
						<td><input style="width:90px;" id="captcha" type="text" class="text" name="captcha" size="6" placeholder="type captcha" maxlength="6"></td>
					</tr>
					<tr>
						<td></td>   
						<td><input type="submit" name="submit" id="submit" value="Submit" class="button"/>&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="reset" name="reset" id="reset" value="Reset" class="button"/></td>
					</tr>

				</table>
               	</form>
			</div>
		</div>  

		
	</div>
	<div id="footer">
		<?php
			include "part/footer.php";
		?>
	</div>

</body>
</html>