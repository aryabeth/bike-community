<?php
	session_start();
?>	

<!DOCTYPE html>
<html>
<head>
	<title>Bike Club Community - BMX</title>
	<?php
		include "part/head.php";
	?>
</head>

<body>
	<div id="container">
		<div id="login">
			<?php
				include "part/topside.php"
			?>
		</div>
		<div id="header">
			<?php
				include "part/header.php";
			?>
		</div>

		<div id="slideshow">
			<?php
				include "part/slideshow.php";
			?>
		</div>

		<div id="sidebar">
			<?php
				include "part/sidebar.php";
			?>
		</div>

		<div id="content">
			<div class="isi">
				<h2> ADD THREAD </h2><hr>
				<div id="inputEvent">
				<form action="addThreadMountain.php" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<td>Judul</td>
							<td><input type="text" name="judulThread" class="textinput" required></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td><textarea name="isi" rows="4" cols="50" class="textinput" required></textarea></td>
						</tr>
						
						<tr>
							<td></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="submit" name="submit" id="submit" value="Submit" class="button"/>&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="reset" name="reset" id="reset" value="Reset" class="button"/></td>
						</tr>
					</table>

					</form>
				</div>
			</div>
		</div>
	</div>
	
	<div id="footer">
		<?php
			include "part/footer.php";
		?>
	</div>

</body>
</html>