<?php
	session_start();
	define('INCLUDE_CHECK',1);

	require "functions.php";
	require "koneksi.php";

	// // remove tweets older than 1 hour to prevent spam
	// mysql_query("DELETE FROM timeline WHERE id>1 AND dt<SUBTIME(NOW(),'0 1:0:0')");

	//fetch the timeline
	$q = mysql_query("SELECT * FROM timeline ORDER BY ID DESC");

	$timeline="";
	while($row=mysql_fetch_assoc($q))
	{
	    $timeline.=formatTweet($row['tweet'],$row['dt']);
	}
	// fetch the latest tweet
	// $lastTweet = '';
	// list($lastTweet) = mysql_fetch_array(mysql_query("SELECT tweet FROM timeline ORDER BY id DESC LIMIT 1"));

	// if(!$lastTweet) $lastTweet = "You don't have any tweets yet!";


?>	

<!DOCTYPE html>
<html>
<head>
	<title>Bike Club Community - Event</title>
	<?php
		include "part/head.php";
	?>
</head>
<script type="text/javascript" >
$(document).ready(function()
{

$(".sttext").livequery(function () { $(this).linkify({
    target: "_blank"
}); });

$('body').on("click",".stdelete",function()
{
var A=$(this).parent().parent();
A.addClass("effectHinge");
A.delay(500).fadeOut("slow",function(){
$(this).remove();
});

});


});
</script>

<style type="text/css">
	
#isi
{
margin:0 auto;
width:800px;
}
#updates
{
position:relative;
padding:20px 0px 20px 0px;
}
#updates:before
{
content: '';
position: absolute;
top: 0px;
bottom: 0;
width: 5px;
background: #999 ;
margin: 0;
}
#updates b{color:#006699}
.timeline_square
{
width:12px;
height:12px;
display:block;
position: absolute;
left:-4px;
border-top:3px solid #e8eaed;
border-bottom:3px solid #e8eaed;
margin-top:-8px;
}
.stbody
{
min-height:50px;
background-color:#ffffff;
padding:10px;
width:600px;
margin-left:30px;
margin-bottom:10px;
border: 0px solid #dfdfdf;
box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1);
}
.stimg
{
float:left;
height:50px;
width:50px;
border:solid 1px #dedede;
padding:2px;
}
.sttext
{
margin-left:70px;

min-height:50px;
word-wrap:break-word;
overflow:hidden;
padding:5px;
display:block;
font-family:'Georgia', Times New Roman, Times, serif
}
.sttime
{
font-size:11px;
color:#999;
font-family:Arial, Helvetica, sans-serif;
margin-top:5px;
}
.stimg img
{
width:50px;height:50px
}
.stdelete
{
float:right;
cursor:pointer;
font-family:arial;
}
.stexpand
{
margin-top:5px;
}
.color1
{
background-color:#f37160
}
.color2
{
background-color:#50b848
}
.color3
{
background-color:#f39c12
}
.color4
{
background-color:#0073b7
}
.color5
{
background-color:#00acd6
}
.effectHinge
{
animation:hinge 1s;
-webkit-animation:hinge 1s; /* Safari and Chrome */
}
</style>
<body>
	<div id="container">
		<div id="login">
			<?php
				include "part/topside.php"
			?>
		</div>
		<div id="header">
			<?php
				include "part/header.php";
			?>
		</div>

		<div id="slideshow">
			<?php
				include "part/slideshow.php";
			?>
		</div>

		<div id="sidebar">
			<?php
				include "part/sidebar.php";
			?>
		</div>

		<div id="content">
			<div class="isi">
				<div id="updates"> 
				<ul class="statuses"><?=$timeline?>
					
						
						<?php 
							if(is_string($dt)) $dt=strtotime($dt);
							$query = "SELECT * FROM timeline ORDER BY id DESC";
							$result = mysql_query($query);
							while($rows = mysql_fetch_assoc($result))
							{
							$tweet=$rows['tweet'];
							$dt=strtotime($rows['dt']);
							echo '<li>
							<div class="stbody">
								<span class="timeline_square color1"></span>
								<div class="stimg"><img src="upload/'.$rows['foto'].'" /></div>
								<div class="sttext">
								<span class="stdelete" title="Delete">X</span>
								<a href="profile.php?id='.$rows['id_member'].'"><b>'.$rows['nama'].'</b></a><br/>
								'.$rows['community'].'
								<div class="sttime">'.relativeTime($dt).'</div>
								<div class="stexpand">'
								. preg_replace('/((?:http|https|ftp):\/\/(?:[A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?[^\s\"\']+)/i','<a href="$1" rel="nofollow" target="blank">$1</a>',$tweet).'
							</div>
							</div></div></li>';
							
						}


						?>
				</ul>
				
				</div>		
			</div>
		</div>

		
	</div>
	<div id="footer">
		<?php
			include "part/footer.php";
		?>
	</div>
	


</body>
</html>