<?php
	session_start();
?>	

<!DOCTYPE html>
<html>
<head>
	<title>Bike Club Community - Home</title>
	
	<?php
		include "part/head.php";
	?>
</head>

<body>
	<div id="container">
		<div id="login">
			<?php
				include "part/topside.php"
			?>
		</div>
		<div id="header">
			<?php
				include "part/header.php";
			?>
		</div>

		<div id="slideshow">
			<?php
				include "part/slideshow.php";
			?>
		</div>

		<div id="sidebar">
			<?php
				include "part/sidebar.php";
			?>
		</div>

		<div id="content">
				<div id="leftcontent"><h3>Hot Items</h3><hr>
				<?php
					$q=mysql_query("SELECT * FROM penjualan ORDER BY id_barang DESC limit 3");
					
					while($data=mysql_fetch_assoc($q)){
						echo "<div class='hotitemnews'>
						<img src='upload/".$data['foto']."' alt='item1'>
						<ul>		
							<li><h4>".$data['nama_barang']." </h4></li>
							<li>Rp. ".$data['harga']."</li>
							<br><h5><a href='detailBarang.php?id=".$data['id_barang']."'>Click for details .. </a></h5>
						</ul>
					</div>";

					}

				?>	

				</div>

				<div id="rightcontent"><h3>Hot Event</h3><hr>
					<?php
						$a=mysql_query("SELECT * FROM event ORDER BY id_event DESC limit 3");
						
						while($rows=mysql_fetch_assoc($a)){
							echo "<div class='hotitemnews'>
							<img src='upload/".$rows['foto']."' alt='event'>
							<ul>		
								<li><h4>".$rows['judul_event']." </h4></li>
								<li>".$rows['tgl']." @".$rows['tempat']."</li>
								<br><h5><a href='event.php'>Click for details .. </a></h5>
							</ul>
							</div>";

						}

					?>	

				</div>
		</div>
		
	</div>

	<div id="footer">
		<?php
			include "part/footer.php";
		?>
	</div>

</body>
</html>