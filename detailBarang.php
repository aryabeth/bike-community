<?php
	session_start();
?>	

<!DOCTYPE html>
<html>
<head>
	<title>Bike Club Community - BMX</title>
	<?php
		include "part/head.php";
	?>
</head>

<body>
	<div id="container">
		<div id="login">
			<?php
				include "part/topside.php"
			?>
		</div>
		<div id="header">
			<?php
				include "part/header.php";
			?>
		</div>

		<div id="slideshow">
			<?php
				include "part/slideshow.php";
			?>
		</div>

		<div id="sidebar">
			<?php
				include "part/sidebar.php";
			?>
		</div>

		<div id="content">
			<div class="isi">
			<?php
				include "koneksi.php";
				$name = $_SESSION['name'];
				$sql = mysql_fetch_assoc(mysql_query("SELECT * FROM member where username ='$name'"));
				$id_org = $sql['id_member'];

				$id_barang = $_GET['id'];
				$q = mysql_query("SELECT * FROM penjualan where id_barang =".$id_barang);
				$data = mysql_fetch_assoc($q);
			?>
				<h2> <?php echo $data['nama_barang'];?>  </h2><hr>

				<div class="fotoBarang">
					<?php
						echo "<img src='upload/".$data['foto']."' alt='barang jualan'>";
					?>
				</div>

				<div class="detailBarang">
					<table border="1">
						<tr>
							<td>Harga</td>
							<td>Rp. <?php echo $data['harga'];?></td>
						</tr>
						<tr>
							<td>Stock</td>
							<td><?php echo $data['stock'];?></td>
						</tr>
						<tr>
							<td>Nomor yang bisa dihubungi</td>
							<td><?php echo $data['no_hp'];?></td>
						</tr>
						<tr>
							<td>Penjual</td>
							<td><?php echo $data['oleh'];?></td>
						</tr>
						<tr>
							<td>Deskripsi</td>
						</tr>
						<tr>
							<td colspan="3"><?php echo $data['deskripsi'];?></td>
						</tr>
					</table>
					<br>
					<?php
						if (isset($_SESSION['name'])){
							echo '<a href="myCart.php?id='.$id_barang.'&idcart='.$id_org.'" class="button">Add to cart</a>';
						}
						if ($_SESSION['name'] == $data['oleh']) {
							echo '&nbsp;&nbsp;&nbsp;&nbsp;<a href="delete.php?id='.$id_barang.'" class="button">Delete</a>';
							
						}
						
					?>
				</div>
				
			
			</div>
		</div>
	</div>
	
	<div id="footer">
		<?php
			include "part/footer.php";
		?>
	</div>

</body>
</html>