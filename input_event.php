<?php
	session_start();
	if(isset($_SESSION['name']))
	{
	}else{
		header( "Location:event.php");
		echo "mohon login dahulu";
	}
?>	

<!DOCTYPE html>
<html>
<head>
	<title>Bike Club Community - Event</title>
	<?php
		include "part/head.php";
	?>
</head>

<body>
	<div id="container">
		<div id="login">
			<?php
				include "part/topside.php"
			?>
		</div>
		<div id="header">
			<?php
				include "part/header.php";
			?>
		</div>

		<div id="slideshow">
			<?php
				include "part/slideshow.php";
			?>
		</div>

		<div id="sidebar">
			<?php
				include "part/sidebar.php";
			?>
		</div>

		<div id="content">
			<div class="isi"> 
				<h2> INPUT EVENT </h2><hr>
				<div id="inputEvent">
				<form action="addEvent.php" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<td>Judul Event</td>
							<td><input type="text" name="judulEvent" class="textinput" required></td>
						</tr>
						<tr>
							<td>Tanggal</td>
							<td><input type="text" name="tglEvent" class="textinput" required></td>
						</tr>
						<tr>
							<td>Waktu</td>
							<td><input type="text" name="waktuEvent" class="textinput" required></td>
						</tr>
						</tr>
							<td>Tempat</td>
							<td><input type="text" name="tempatEvent" class="textinput"required></td>
						</tr>
						<tr>
							<td>Foto</td>
							<td><input type="file" name="fotoEvent" class="textinput"></td>
						</tr>
						<tr>
							<td>Deskripsi</td>
							<td><textarea name="deskEvent"rows="4" cols="50" class="textinput" required></textarea></td>
						</tr>
						
						<tr>
							<td></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="submit" name="submit" id="submit" value="Submit" class="button"/>&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="reset" name="reset" id="reset" value="Reset" class="button"/></td>
						</tr>
					</table>

					</form>
				</div>
			</div>
		</div>

		
	</div>
	<div id="footer">
		<?php
			include "part/footer.php";
		?>
	</div>
	
</body>
</html>