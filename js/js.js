$(document).ready( function(){
	
	$(".comunity").hide();
	$("#formlogin").hide();

	$("#tweetForm").hide();
	$("#btnCom").click(function(){
		$(".comunity").slideToggle();
	});

	$("#search").focus(function(){
		var a = $("#search").width();
		if(a>=98 && a<=99){
			$("#search").animate({
			     width:'+=50px'
		 	});
	 	}
	});

	$("#search").blur(function(){
		var a = $("#search").width();
		if(a>=148 && a <=149){
			$("#search").animate({
			     width:'-=50px'
		 	});
	 	}
	});

	$("#log").click(function(){
		$("#formlogin").slideToggle();
	});

	$("input[type=username]").focus( function() {
      //jQuery UI doesn't support the hotpink keyword 
      $(this).css("box-shadow","0 0 1px 1px green");
	 });

	$("input[type=username]").blur( function() {
      //jQuery UI doesn't support the hotpink keyword 
      $(this).css("box-shadow","none");
	 });

	$("input[type=password]").focus( function() {
      //jQuery UI doesn't support the hotpink keyword 
      $(this).css("box-shadow","0 0 1px 1px green");
	 });

	$("input[type=password]").blur( function() {
      //jQuery UI doesn't support the hotpink keyword 
      $(this).css("box-shadow","none");
	 });

	$('button[id=signup').on('mouseover', function() {
      //jQuery UI doesn't support the hotpink keyword 
      $(this).animate({ backgroundColor: "white", color : "green" }, 500)
			  }).on('mouseleave', function() {
			      $(this).stop(true, true);
			      $(this).animate({ backgroundColor: "green", color : "white" }, 500);
	 }); 

	$('input[name=login]').on('mouseover', function() {
      //jQuery UI doesn't support the hotpink keyword 
      $(this).animate({ backgroundColor: "white", color : "green" }, 500)
			  }).on('mouseleave', function() {
			      $(this).stop(true, true);
			      $(this).animate({ backgroundColor: "green", color : "white" }, 500);
	 }); 
	
	$('input[name=submit]').on('mouseover', function() {
      //jQuery UI doesn't support the hotpink keyword 
      $(this).animate({ backgroundColor: "white", color : "green" }, 500)
			  }).on('mouseleave', function() {
			      $(this).stop(true, true);
			      $(this).animate({ backgroundColor: "green", color : "white" }, 500);
	 }); 
	
	$('input[name=reset]').on('mouseover', function() {
      //jQuery UI doesn't support the hotpink keyword 
      $(this).animate({ backgroundColor: "white", color : "green" }, 500)
			  }).on('mouseleave', function() {
			      $(this).stop(true, true);
			      $(this).animate({ backgroundColor: "green", color : "white" }, 500);
	 }); 

	$('input[name=reset2]').on('mouseover', function() {
      //jQuery UI doesn't support the hotpink keyword 
      $(this).animate({ backgroundColor: "white", color : "green" }, 500)
			  }).on('mouseleave', function() {
			      $(this).stop(true, true);
			      $(this).animate({ backgroundColor: "green", color : "white" }, 500);
	 }); 

	$('input[name=Submit]').on('mouseover', function() {
      //jQuery UI doesn't support the hotpink keyword 
      $(this).animate({ backgroundColor: "white", color : "green" }, 500)
			  }).on('mouseleave', function() {
			      $(this).stop(true, true);
			      $(this).animate({ backgroundColor: "green", color : "white" }, 500);
	 }); 



	$('.menu a').on('mouseover', function() {
      //jQuery UI doesn't support the hotpink keyword 
      $(this).animate({ backgroundColor: "#c8ced4", color : "green"  }, 200)
			  }).on('mouseleave', function() {
			      $(this).stop(true, true);
			      $(this).animate({ backgroundColor: "#333333", color : "white" }, 200);
	 }); 

	$('.comunity a').on('mouseover', function() {
      //jQuery UI doesn't support the hotpink keyword 
      $(this).animate({ backgroundColor: "#c8ced4", color : "green"  }, 200)
			  }).on('mouseleave', function() {
			      $(this).stop(true, true);
			      $(this).animate({ backgroundColor: "#333333", color : "white" }, 200);
	 }); 

	$('#btnCom').on('mouseover', function() {
      //jQuery UI doesn't support the hotpink keyword 
      $(this).animate({ backgroundColor: "#c8ced4", color : "green"  }, 200)
			  }).on('mouseleave', function() {
			      $(this).stop(true, true);
			      $(this).animate({ backgroundColor: "#333333", color : "white" }, 200);
	 }); 


	$("#log").on('mouseover', function() {
      //jQuery UI doesn't support the hotpink keyword 
      $(this).animate({ backgroundColor: "white", color : "green" }, 500)
			  }).on('mouseleave', function() {
			      $(this).stop(true, true);
			      $(this).animate({ backgroundColor: "#55B05A", color : "black" }, 500);
	 });

	$("#logout").on('mouseover', function() {
      //jQuery UI doesn't support the hotpink keyword 
      $(this).animate({ backgroundColor: "white", color : "green" }, 500)
			  }).on('mouseleave', function() {
			      $(this).stop(true, true);
			      $(this).animate({ backgroundColor: "#55B05A", color : "black" }, 500);
	 });

	$("#notificationLink").click(function(){

		$("#notificationContainer").fadeToggle(300);
		$("#notification_count").fadeOut("slow");
			return false;
	});


	$("#status").click(function(){
		$("#tweetForm").fadeToggle(300);
			return false;	
	});

			//Document Click
	$(document).click(function(){
		$("#notificationContainer").hide();

		// $("#statusContainer").hide();
	});
			//Popup Click
	$("#notificationContainer").click(function(){
		return false
	});


	 $('#tweetForm').submit(function(e){

        tweet();
        e.preventDefault();

    });
});

function tweet()
{
    var submitData = $('#tweetForm').serialize();

    $.ajax({
        type: "POST",
        url: "submit.php",
        data: submitData,
        dataType: "html",
        success: function(msg){

            if(parseInt(msg)!=0)
            {
                $('ul.statuses li:first-child').before(msg);
				$("ul.statuses:empty").append(msg);

                // $('#lastTweet').html($('#inputField').val());

                $('#statuspost').val('');
            }
        }

    });

}

function comment()
{
    var submitData = $('#tweetForm').serialize();

    $.ajax({
        type: "POST",
        url: "submit.php",
        data: submitData,
        dataType: "html",
        success: function(msg){

            if(parseInt(msg)!=0)
            {
                $('ul.statuses li:first-child').before(msg);
				$("ul.statuses:empty").append(msg);

                // $('#lastTweet').html($('#inputField').val());

                $('#statuspost').val('');
            }
        }

    });

}